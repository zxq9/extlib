%%% @doc
%%% Parallel Processing Patterns: ppp
%%%
%%% 
%%% @end

-module(ppp).
-vsn("1.0.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("MIT").

-export([tmap/3, ctmap/3, ctmap/4,
         nmap/2, cnmap/2, cnmap/3]).

-spec tmap(Function, List, Timeout) -> Result
    when Function :: fun((term()) -> {ok, term()} | {error, term()}),
         List     :: list(),
         Timeout  :: infinity | non_neg_integer(),
         Result   :: {ok,      Results    :: [term()]}
                   | {done,    Results    :: [term()],
                               Errors     :: [{Input :: term(), Error :: term()}]}
                   | {timeout, Results    :: [term()],
                               Errors     :: [{Input :: term(), Error :: term()}],
                               Incomplete :: [term()]}.
%% @doc
%% Thorough parallel map implementation.
%%
%% This implementation covers the cases of:
%%  - complete execution
%%  - partial execution with full error information retention
%%  - partial execution in the context of a provided timeout
%% All of these cases can be referenced by their original inputs and will retain
%% their relative sequence as provided in the initial input list.
%%
%% Note that this version is not bounded by any hardware limitations, leaving
%% efficiency concerns entirely up to the Erlang scheduler to handle. This is
%% usually, but not always, what you want.

tmap(Function, List, Timeout) ->
    {Workers, Count} = work(execute(Function), List, [], 0),
    Timer =
        case Timeout of
            infinity -> none;
            MilliSec -> erlang:send_after(MilliSec, self(), timeout)
        end,
    gather(Workers, Count, Timer).

work(_, [], A, C) ->
    {A, C};
work(E, [H | T], A, C) ->
    work(E, T, [E(H) | A], C + 1).

execute(F) ->
    fun(E) ->
        P = spawn_monitor(fun() -> exit(F(E)) end),
        {pending, P, E}
    end.

gather(Pending, Count, Timer) when Count > 0 ->
    receive
        {'DOWN', _, process, PID, Outcome} ->
            Received = complete(PID, Outcome, Pending),
            gather(Received, Count - 1, Timer);
        timeout ->
            the_final_solution(Pending)
    end;
gather(Results, 0, none) ->
    the_final_solution(Results);
gather(Results, 0, Timer) ->
    _ = erlang:cancel_timer(Timer),
    the_final_solution(Results).

complete(PID, Result, [{pending, {PID, _}, Input} | Rest]) ->
    [{done, Input, Result} | Rest];
complete(PID, Result, [Element | Rest]) ->
    [Element | complete(PID, Result, Rest)].

the_final_solution(Results) ->
    case lists:foldl(fun finalize/2, {[], [], []}, Results) of
        {Final, [], []}             -> {ok, Final};
        {Final, Errors, []}         -> {done, Final, Errors};
        {Final, Errors, Incomplete} -> {timeout, Final, Errors, Incomplete}
    end.

finalize({done, _, {ok, Result}}, {Results, Errors, Incomplete}) ->
    {[Result | Results], Errors, Incomplete};
finalize({done, Input, Error}, {Results, Errors, Incomplete}) ->
    {Results, [{Input, Error} | Errors], Incomplete};
finalize({pending, {PID, Mon}, Input}, {Results, Errors, Incomplete}) ->
    true = exit(PID, kill),
    true = demonitor(Mon, [flush]),
    {Results, Errors, [Input | Incomplete]}.


-spec ctmap(Fun, List, Timeout) ->

-spec ctmap(Fun, List, Timeout, MaxWorkers) ->


-spec nmap(Fun, List) -> Results
    when Fun     :: fun((any()) -> any()),
         List    :: list(),
         Results :: list().
%% @doc
%% Naive Linked Map implementation.
%%
%% A simple, naive implementation of a parallel map that preserves list order
%% and crashes all workers in the event any operation fails. The complete nuking
%% of any accidentally toxic operations is a great feature, not a bug.
%%
%% Despite the guarantees of mutual sucide among every process in the parallel
%% processing pool generated by this function, this simple implementation is
%% fast, limits the scope of errors in a very strict way, and is very composable
%% on the calling side.

nmap(F, L) ->
    Parent = self(),
    Pids = [spawn_link(fun() -> Parent ! {self(), F(X)} end) || X <- L],
    [receive {Pid, Res} -> Res end || Pid <- Pids].


-spec cnmap(Fun, List) -> Results


-spec cnmap(Fun, List, MaxWorkers) -> Results



-spec vcore_count() -> non_neg_integer().

vcore_count() ->
    case erlang:system_info(cpu_topology) of
        undefined ->
            Count = erlang:system_info(logical_processors_available),
            ok = tell("Core count: ~w", [Count]),
            Count;
        Topology ->
            Count = vcore_count(Topology, 0),
            ok = tell("Core count: ~w", [Count]),
            Count
    end.

vcore_count([], C) ->
    C;
vcore_count([{_, {logical, N}} | T], C) when is_integer(N) ->
    vcore_count(T, C + 1);
vcore_count([{_, Sub} | T], C) when is_list(Sub) ->
    vcore_count(T, vcore_count(Sub, C));
vcore_count([{_, _, {logical, N}} | T], C) when is_integer(N) ->
    vcore_count(T, C + 1);
vcore_count([{_, _, Sub} | T], C) when is_list(Sub) ->
    vcore_count(T, vcore_count(Sub, C)).


-spec pcore_count() -> non_neg_integer().
